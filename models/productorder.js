'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class ProductsOrders extends Model {
        static associate(models) {
            models.ProductsOrders.belongsTo(models.Product, { foreignKey: 'productId' });
            models.ProductsOrders.belongsTo(models.Order, { foreignKey: 'orderId' });
        }
    };
    ProductsOrders.init({
        productId: DataTypes.INTEGER,
        orderId: DataTypes.INTEGER,
        numberOfItems: DataTypes.INTEGER,
        totalPrice: DataTypes.DOUBLE,
    }, {
        sequelize,
        modelName: 'ProductsOrders',
    });
    return ProductsOrders;
};