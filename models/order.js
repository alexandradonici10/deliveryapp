'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Order extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      models.Order.belongsTo(models.User, { foreignKey: 'userId' });
      models.Order.belongsToMany(models.Product, {
        through: 'ProductsOrders',
      });
    }
  };
  Order.init({
    userId: DataTypes.INTEGER,
    orderCost: DataTypes.DOUBLE,
    deliveryLocation: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Order',
  });
  return Order;
};