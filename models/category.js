'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Category extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      models.Category.belongsTo(models.Restaurant, { foreignKey: 'restaurantId' });
      models.Category.hasMany(models.Product, { foreignKey: 'categoryId' });
    }
  };
  Category.init({
    name: DataTypes.STRING,
    restaurantId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Category',
  });
  return Category;
};