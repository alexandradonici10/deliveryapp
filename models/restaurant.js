'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Restaurant extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      models.Restaurant.hasMany(models.Category, { foreignKey: 'restaurantId' });
      models.Restaurant.belongsToMany(models.User, {
        through: 'UsersRestaurants',
      });

    }
  };
  Restaurant.init({
    name: DataTypes.STRING,
    minimumOrderAmount: DataTypes.INTEGER,
    deliveryCosts: DataTypes.DOUBLE,
    deliveryTime: DataTypes.INTEGER,
    description: DataTypes.STRING,
    location: DataTypes.STRING,
    rating: DataTypes.DOUBLE
  }, {
    sequelize,
    modelName: 'Restaurant',
  });
  return Restaurant;
};