'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const restaurants = [
      {
        name: 'Tasty Box',
        minimumOrderAmount: 30,
        deliveryCosts: 0,
        deliveryTime: 60,
        description: 'This restaurant provides stamps. To receive them via email, please make sure you\'re subscribed to our newsletter by checking the box for discounts, new restaurants updates and special offers at checkout.',
        location: 'Bucureştii Noi, nr. 179, sector 1, Bucuresti',
        rating: 0,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Pizza Hut',
        minimumOrderAmount: 40,
        deliveryCosts: 5,
        deliveryTime: 35,
        description: '',
        location: 'Bulevardul Ion Mihalache 93-105, București',
        rating: 0,
        createdAt: new Date(),
        updatedAt: new Date(),
      }];
    await queryInterface.bulkInsert('Restaurants', restaurants, {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Restaurants', null, {});
  }
};
