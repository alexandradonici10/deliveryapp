const { GraphQLInputObjectType, GraphQLString, GraphQLInt, GraphQLList } = require('graphql');

const itemInputType = new GraphQLInputObjectType({
  name: 'ItemInput',
  fields: {
    productId: { type: GraphQLInt },
    numberOfItems: { type: GraphQLInt },
  }
});

module.exports = itemInputType;