const { GraphQLInputObjectType, GraphQLString, GraphQLInt, GraphQLList, GraphQLNonNull } = require('graphql');

const userInputType = new GraphQLInputObjectType({
  name: 'UserInput',
  fields: {
    email: {
        type: GraphQLNonNull(GraphQLString),
    },
    firstName: {
        type: GraphQLNonNull(GraphQLString),
    },
    lastName: {
        type: GraphQLNonNull(GraphQLString),
    },
    phone: {
        type: GraphQLNonNull(GraphQLString),
    },
    password: {
        type: GraphQLNonNull(GraphQLString),
    },
  }
});

module.exports = userInputType;