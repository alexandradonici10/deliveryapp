const { GraphQLInputObjectType, GraphQLString, GraphQLInt, GraphQLList, GraphQLSchema, GraphQLFloat } = require('graphql');

const categoryInputType = new GraphQLInputObjectType({
  name: 'CategoryInput',
  fields: {
    name: { type: GraphQLString },
    restaurantId: {type: GraphQLInt},
  }
});

module.exports = categoryInputType;