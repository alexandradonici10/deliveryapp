const { GraphQLObjectType, GraphQLNonNull, GraphQLInt, GraphQLString, GraphQLFloat, GraphQLList } = require('graphql');
const models = require('../models');
const productType = require('./types/productType');
const productInputType = require('./inputTypes/productInputType');
const orderType = require('./types/orderType');
const orderInputType = require('./inputTypes/orderInputType');
const categoryType = require('./types/categoryType');
const categoryInputType = require('./inputTypes/categoryInputType');
const userInputType = require('./inputTypes/userInputType');
const config = require('../config/config');
const jwt = require('jsonwebtoken');
const appConfig = require('../config/appConfig');
const bcrypt = require('bcrypt');

const mutationType = new GraphQLObjectType({
    name: 'Mutation',
    fields: {

        addProducts: {
            type: GraphQLList(productType),
            args: {
                productsInput: {
                    type: GraphQLList(productInputType)
                },
            },
            resolve: async (_, { productsInput }) => {

                var products = models.Product.bulkCreate(productsInput);
                return products;

            }
        },

        addCategories: {
            type: GraphQLList(categoryType),
            args: {
                categoriesInput: {
                    type: GraphQLList(categoryInputType)
                },
            },
            resolve: async (_, { categoriesInput }) => {
                var categories = models.Category.bulkCreate(categoriesInput);
                return categories;
            }
        },

        addOrder: {
            type: orderType,
            args: {
                orderInput: {
                    type: GraphQLNonNull(orderInputType)
                },
            },
            resolve: async (_, { orderInput }, context) => {

                let order = await models.Order.create({
                    userId: context.user.id,
                    deliveryLocation: orderInput.deliveryLocation,
                });

                const items = orderInput.orderProducts;

                const products = await models.Product.findAll({
                    where: {
                        id: items.map(p => p.productId)
                    }
                });

                const productsOrders = products.map((product, i) => {
                    return {
                        orderId: order.id,
                        productId: product.id,
                        numberOfItems: items[i].numberOfItems,
                        totalPrice: items[i].numberOfItems * product.price
                    }
                });

                await models.ProductsOrders.bulkCreate(productsOrders);

                const orderPrice = productsOrders.reduce((acc, p) => acc + p.totalPrice, 0);
                order.orderCost = orderPrice;
                order.save();

                return order;
            }
        },

        deleteProduct: {
            type: GraphQLString,
            args: {
                productId: {
                    type: GraphQLNonNull(GraphQLInt)
                }
            },
            resolve: async (_, { productId }) => {
                let product = await models.Product.findByPk(productId);

                if (!productId) {
                    throw "Product not found!";
                }

                await product.destroy();
                return "The selected product was deleted!";
            },
        },

        deleteCategory: {
            type: GraphQLString,
            args: {
                categoryId: {
                    type: GraphQLNonNull(GraphQLInt)
                }
            },
            resolve: async (_, { categoryId }) => {
                let category = await models.Category.findByPk(categoryId);

                if (!categoryId) {
                    throw "Category not found!";
                }

                models.Product.destroy({
                    where: {
                        categoryId: categoryId
                    }
                });

                await category.destroy();
                return "The selected category and its associated products was deleted!";
            },
        },

        login: {
            type: GraphQLString,
            args: {
                email: {
                    type: GraphQLNonNull(GraphQLString),
                },
                password: {
                    type: GraphQLNonNull(GraphQLString),
                },
            },
            resolve: async (parent, { email, password }) => {
                const user = await models.User.findOne({
                    where: {
                        email,
                    }
                });

                if (user) {
                    const isValid = await bcrypt.compare(password, user.password);

                    if (isValid) {
                        const token = jwt.sign({ userId: user.id }, appConfig.JWTSECRET);
                        return token;
                    }
                }
                return null;
            }
        },

        register: {
            type: GraphQLString,
            args: {
                userInput: {
                    type: GraphQLNonNull(userInputType)
                },
            },
            resolve: async (_, { userInput }) => {

                userInput.password = await bcrypt.hash(userInput.password, appConfig.SALT_ROUNDS);
                userInput.userName = `${userInput.firstName}-${userInput.lastName}`;

                const user = await models.User.create(userInput);
                return user.password;
            }
        }
    }
});

module.exports = mutationType;