const { GraphQLObjectType, GraphQLInt, GraphQLString, GraphQLList } = require('graphql');
const restaurantType = require('./restaurantDtoType');
const productType = require('./productType');
const models = require('../../models');

const categoryType = new GraphQLObjectType({
  name: 'Category',
  fields: () => ({
    id: { type: GraphQLInt },
    name: { type: GraphQLString },
    restaurant: { 
        type: restaurantType,
        resolve: async (parent) => {
          return await parent.getRestaurant();
        }
    },
    products: {
      type: new GraphQLList(productType),
      resolve: async (parent) => {
        return await parent.getProducts();
      }
    },
    createdAt: { type: GraphQLString },
    updatedAt: { type: GraphQLString },
    
  })
});

module.exports = categoryType;