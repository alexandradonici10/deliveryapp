const { GraphQLObjectType, GraphQLInt, GraphQLString, GraphQLList, GraphQLFloat } = require('graphql');
const userType = require('./userType');
const models = require('../../models');

const orderType = new GraphQLObjectType({
  name: 'Order',
  fields: () => ({
    id: { type: GraphQLInt },
    orderCost: { type: GraphQLFloat },
    deliveryLocation: { type: GraphQLString },
    user: {
      type: userType,
      resolve: async (parent) => {
        return await parent.getUser();
      }
    },
    createdAt: { type: GraphQLString },
    updatedAt: { type: GraphQLString },
  })
});

module.exports = orderType;