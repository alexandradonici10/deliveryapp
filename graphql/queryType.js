const { GraphQLObjectType, GraphQLNonNull, GraphQLInt, GraphQLList } = require('graphql');
const models = require('../models');
const userType = require('./types/userType');
const restaurantType = require('./types/restaurantType');
const categoryType = require('./types/categoryType');

const productType = require('./types/productType');
const orderType = require('./types/orderType');

const queryType = new GraphQLObjectType({
  name: 'Query',
  fields: {
    user: {
      type: userType,
      args: {
        userId: {
          type: GraphQLNonNull(GraphQLInt)
        }
      },
      resolve: async (_, { userId }) => {
        const user = await models.User.findByPk(userId);
        return user;
      }
    },
    restaurant: {
      type: restaurantType,
      args: {
        restaurantId: {
          type: GraphQLNonNull(GraphQLInt)
        }
      },
      resolve: async (_, { restaurantId }) => {
        const restaurant = await models.Restaurant.findByPk(restaurantId);
        return restaurant;
      }
    },
    category: {
      type: categoryType,
      args: {
        categoryId: {
          type: GraphQLNonNull(GraphQLInt)
        }
      },
      resolve: async (_, { categoryId }) => {
        const category = await models.Category.findByPk(categoryId);
        return category;
      }
    },
    userOrders: {
      type: GraphQLList(orderType),
      args: {
      },
      resolve: async (_, args, context) => {
        const currentUser = context.user;
        const orders = await currentUser.getOrders();
        return orders;
      }
    }
  }
});

module.exports = queryType;